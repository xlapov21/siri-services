CREATE TABLE IF NOT EXISTS posts
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    type        TEXT,
    name        TEXT NOT NULL,
    description TEXT NOT NULL,
    schedule    TEXT NOT NULL,
    author_id     INT REFERENCES users NOT NULL,
    check (type in ('employee', 'employer'))
);