DROP TABLE IF EXISTS users CASCADE;
DROP TABLE IF EXISTS reviews CASCADE;
DROP TABLE IF EXISTS posts CASCADE;
DROP TABLE IF EXISTS FAQs CASCADE;
DROP TABLE IF EXISTS newQuestions CASCADE;
DROP TABLE IF EXISTS wishlist CASCADE;
DROP TABLE IF EXISTS orders CASCADE;

CREATE TABLE IF NOT EXISTS users
(
    id       integer PRIMARY KEY
        UNIQUE
                  NOT NULL GENERATED ALWAYS AS IDENTITY,
    name            text NOT NULL,
    login           text NOT NULL,
    password        text NOT NULL,
    phone_number    text NOT NULL
);

CREATE TABLE IF NOT EXISTS reviews
(
    id          integer PRIMARY KEY
        UNIQUE
                    NOT NULL GENERATED ALWAYS AS IDENTITY,
    topic           TEXT NOT NULL,
    description     TEXT NOT NULL,
    author_id       INT REFERENCES users NOT NULL,
    addressee_user_id         INT REFERENCES users NOT NULL
);

CREATE TABLE IF NOT EXISTS posts
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    type        TEXT,
    name        TEXT NOT NULL,
    description TEXT NOT NULL,
    schedule    TEXT NOT NULL,
    author_id     INT REFERENCES users NOT NULL
--    check (type in ('employee', 'employer'))
);

CREATE TABLE IF NOT EXISTS FAQs
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    question        TEXT NOT NULL,
    answer          TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS newQuestions
(
    id          integer PRIMARY KEY
        UNIQUE
                     NOT NULL GENERATED ALWAYS AS IDENTITY,
    name             TEXT NOT NULL,
    email            TEXT NOT NULL,
    message          TEXT NOT NULL,
    is_answered      BOOLEAN
);

CREATE TABLE IF NOT EXISTS wishlist
(
    user_id     INT REFERENCES users NOT NULL,
    post_id     INT REFERENCES posts NOT NULL
);

CREATE TABLE IF NOT EXISTS orders
(
    user_id     INT REFERENCES users NOT NULL,
    post_id     INT REFERENCES posts NOT NULL
);
