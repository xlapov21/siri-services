from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField
from wtforms.validators import DataRequired


class RegisterForm(FlaskForm):
    login = StringField('login', validators=[DataRequired()])
    name = StringField('name', validators=[DataRequired()])
    password = StringField('password', validators=[DataRequired()])
    phone_number = StringField('phone_number', validators=[DataRequired()])
