from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, SelectField, DateField
from wtforms.validators import DataRequired
import datetime


class CreatePostForm(FlaskForm):
    name_choices = ['employee', 'employeer']
    type = SelectField('Тип', choices = name_choices)
    name = StringField('Название')
    description = StringField('Описание')
    schedule = StringField('Расписание')
    create_submit = SubmitField('Создать')
    exit_submit = SubmitField('Отмена')

