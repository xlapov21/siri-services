from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired


class CreateReviewForm(FlaskForm):
    message = PasswordField('Message', validators=[DataRequired()])
    submit_message = SubmitField('Submit Message')