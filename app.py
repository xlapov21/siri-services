from flask import Flask, render_template, url_for, request, redirect, session, jsonify
from flask_bootstrap import Bootstrap
from forms.login import LoginForm
from forms.register import RegisterForm
from forms.create_post import CreatePostForm
import os
import psycopg
from dotenv import load_dotenv
import i18n


def get_text(address, locale):
    i18n.load_path.append('locales')
    return i18n.t(address, locale=locale)


load_dotenv()

app = Flask(__name__)
bootstrap = Bootstrap(app)
app.config.update(dict(
    SECRET_KEY="powerful secretkey",
    WTF_CSRF_SECRET_KEY="a csrf secret key"
))

conn = psycopg.connect(dbname=os.getenv('DB_NAME'),
                       user=os.getenv('DB_USER'),
                       password=os.getenv('DB_PASSWORD'),
                       host=os.getenv('DB_HOST'),
                       port=os.getenv('DB_PORT'))
conn.autocommit = True

cur = conn.cursor()


def init_db():
    cur.execute(open("migrations/general_init.sql", "r").read())


# init_db()

@app.route("/about_company")
def about_company():
    return render_template("about_company.html", g=get_text)


@app.route("/catalog")
def catalog():
    if 'login' in session:
        type = request.args.get('type')
        cur.execute(
            f"""SELECT * FROM posts WHERE type = '{type}' 
            {f"AND name LIKE '%{request.args.get('search')}%';" if request.args.get('search') else ";"}""")

        return render_template("catalog.html", posts=cur.fetchall(), g=get_text, type=type)
    else:
        return redirect(url_for('login', g=get_text))


@app.route("/FAQ")
def faq():
    return render_template("FAQ.html", g=get_text)


@app.route("/create_post", methods=["POST", "GET"])
def create_post():
    if 'login' in session:
        if request.method == "POST":
            cur.execute(
                f"""INSERT INTO posts (
                             type,
                             name,
                             description,
                             schedule,
                             author_id) VALUES ( 
                             '{request.form['type']}', 
                             '{request.form['name']}', 
                             '{request.form['description']}', 
                             '{request.form['schedule']}', 
                             {session['id']});"""
            )

            return redirect(url_for('catalog', type=request.form['type'], g=get_text))
        elif request.method == "GET":
            form = CreatePostForm()
            return render_template("create_post.html", form=form, g=get_text)
    else:
        return redirect(url_for('login', g=get_text))


def get_user_name(id):
    cur.execute(f"SELECT name FROM users WHERE id = {id};")
    return cur.fetchone()[0]


@app.route("/")
@app.route("/profile")
def profile():
    if 'login' in session:
        id = request.args.get('id') if request.args.get('id') else session['id']
        user_name = get_user_name(id)
        cur.execute(
            f"""SELECT posts.id, posts.type, posts.name, description, schedule
            FROM posts 
            left join users on users.id = author_id 
            WHERE author_id = '{id}';
            """
        )

        posts = cur.fetchall()
        return render_template("profile.html", name=user_name, posts=posts, g=get_text,
                               is_my_profile=int(id) == session['id'], id=id)
    else:
        return redirect(url_for('login', g=get_text))


@app.route("/review", methods=["POST", "GET"])
def review():
    if 'login' in session:
        if request.method == 'POST':
            review_text = ''
        id = request.args.get('id') if request.args.get('id') else session['id']
        user_name = get_user_name(id)
        cur.execute(
            f"""SELECT reviews.topic, reviews.description, name
            FROM reviews 
            left join users on users.id = author_id 
            WHERE addressee_user_id = '{id}';
            """
        )

        reviews = cur.fetchall()
        return render_template("review.html", name=user_name, reviews=reviews, g=get_text,
                               is_my_profile=int(id) == session['id'], id=id)
    else:
        return redirect(url_for('login', g=get_text))


@app.route("/wishlist")
def wishlist():
    if 'login' in session:
        id = request.args.get('id') if request.args.get('id') else session['id']
        user_name = get_user_name(id)
        cur.execute(
            f"""SELECT posts.* FROM posts 
            join wishlist on wishlist.post_id = posts.id
            WHERE wishlist.user_id = {id};""")

        return render_template("wishlist.html", name=user_name, wishlist=cur.fetchall(), g=get_text)
    else:
        return redirect(url_for('login', g=get_text))


@app.route("/basket")
def basket():
    if 'login' in session:
        id = request.args.get('id') if request.args.get('id') else session['id']
        user_name = get_user_name(id)
        basket = ["маникюр", "педикюр"]

        return render_template("basket.html", name=user_name, basket=basket, g=get_text)
    else:
        return redirect(url_for('login', g=get_text))


@app.route("/login", methods=["POST", "GET"])
def login():
    error = None
    if request.method == 'POST':
        cur.execute(
            f"SELECT id FROM users WHERE login = '{request.form['login']}' and password = '{request.form['password']}';")

        user_id = cur.fetchone()
        print(user_id)
        if user_id:
            session['login'] = request.form['login']
            session['id'] = user_id[0]
            return redirect(url_for('profile', id=session['id'], g=get_text))
        else:
            error = 'Неверные данные, попробуйте еще раз'
    return render_template("login.html", form=LoginForm(), error=error, g=get_text)


@app.route("/signup", methods=["POST", "GET"])
def registration():
    if request.method == "POST":
        name = request.form['name']
        login = request.form['login']
        password = request.form['password']
        phone_number = request.form['phone_number']
        cur.execute(f"SELECT id FROM users WHERE login = '{login}';")

        if not cur.fetchone():
            cur.execute(
                f"""INSERT INTO users (
                 login,
                 password,
                 name,
                 phone_number) VALUES ( '{login}', '{password}', '{name}', '{phone_number}') RETURNING id;"""
            )

            session['login'] = login
            session['id'] = cur.fetchone()[0]
            print(session['id'])
        return redirect(url_for('profile', id=session['id'], g=get_text))
    elif request.method == "GET":
        form = RegisterForm()
        return render_template("registration.html", form=form, g=get_text)


@app.route('/logout')
def logout():
    session.pop('login', None)
    session.pop('id', None)
    return redirect(url_for('login', g=get_text))


@app.route('/cart/<int:cart_id>')
def cart(cart_id):
    if 'login' in session:
        cur.execute(
            f"""SELECT posts.id, posts.type, posts.name, description, schedule, users.name 
                    FROM posts 
                    left join users on users.id = author_id 
                    WHERE posts.id = '{cart_id}';""")
        post = cur.fetchall()[0]
        cur.execute(f"SELECT author_id FROM posts WHERE posts.id = {cart_id};")
        author_id = int(cur.fetchone()[0])
        return render_template("cart.html", post=post, g=get_text, author_id=author_id, is_my_cart=author_id == session['id'])
    else:
        return redirect(url_for('login', g=get_text))

@app.route('/cart/delete/<int:cart_id>')
def delete_cart(cart_id):
    if 'login' in session:
        cur.execute(
            f"""DELETE 
                    FROM posts 
                    WHERE posts.id = '{cart_id}';""")

        return render_template("delete_cart.html", g=get_text)
    else:
        return redirect(url_for('login', g=get_text))

@app.route("/api/get_posts")
def api_get_posts():
    cur.execute(
        f"""SELECT * FROM posts 
        WHERE author_id = {request.args.get('id')};""")

    return jsonify(cur.fetchall())


@app.route("/api/create_post")
def api_create_post():
    cur.execute(
        f"""INSERT INTO posts (
                     type,
                     name,
                     description,
                     schedule,
                     author_id) VALUES ( 
                     '{request.args.get('type')}', 
                     '{request.args.get('name')}', 
                     '{request.args.get('description')}', 
                     '{request.args.get('schedule')}', 
                     {request.args.get('id')})
        RETURNING id;""")
    return {'id': cur.fetchone()[0]} if cur.fetchone() else {'error': 'bad request'}


@app.route("/api/create_user")
def api_create_user():
    cur.execute(
        f"""INSERT INTO users (
                 login,
                 password,
                 name,
                 phone_number) 
        VALUES ( 
        '{request.args.get('login')}', 
        '{request.args.get('password')}', 
        '{request.args.get('name')}', 
        '{request.args.get('phone_number')}') 
        RETURNING id;""")
    return {'id': cur.fetchone()[0]} if cur.fetchone() else {'error': 'bad request'}


if __name__ == '__main__':
    app.run(debug=True)
